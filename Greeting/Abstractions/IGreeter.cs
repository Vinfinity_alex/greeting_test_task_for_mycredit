namespace Greeter.Abstractions
{
    public interface IGreeter
    {
        string SayHello();
    }
}