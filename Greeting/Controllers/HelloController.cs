using Greeter.Abstractions;
using Greeter.Services;
using Microsoft.AspNetCore.Mvc;

namespace Greeter.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HelloController : Controller
    {
        private readonly IGreeter _greeterService;
        
        public HelloController(SayHelloService service)
        {
            _greeterService = service;
        }
        
        [HttpGet]
        public string Get()
        {
            return _greeterService.SayHello();
        }
    }
}