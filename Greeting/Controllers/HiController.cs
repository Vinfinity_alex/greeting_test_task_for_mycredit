using Greeter.Abstractions;
using Greeter.Services;
using Microsoft.AspNetCore.Mvc;

namespace Greeter.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HiController : Controller
    {
        private readonly IGreeter _greeterService;
        
        public HiController(SayHiService service)
        {
            _greeterService = service;
        }
        
        [HttpGet]
        public string Get()
        {
            return _greeterService.SayHello();
        }
    }
}