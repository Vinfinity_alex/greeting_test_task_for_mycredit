using Greeter.Abstractions;

namespace Greeter.Services
{
    public class SayHelloService : IGreeter
    {
        public string SayHello()
        {
            return "Hello";
        }
    }
}