using Greeter.Abstractions;

namespace Greeter.Services
{
    public class SayHiService : IGreeter
    {
        public string SayHello()
        {
            return "Hi";
        }
    }
}